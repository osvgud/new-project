var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var path = require('path');

module.exports = {
    entry: {
        index: './src/site/index.js'
},
    output: {
        path: __dirname + "/dotnet/dotnet/static",
        filename: '[name]as.bundle.js'
    },
    module: {
        rules: [
            {
                 test: /\.json$/, loader: 'json-loader' 
            },
            {
                test: /\.scss$/, 
                use: ExtractTextPlugin.extract({
                    fallbackLoader: 'style-loader',
                    loader: ['css-loader','sass-loader'],
                    publicPath: '/dist'
                })
            },
            {
        test: /\.php$/,
        loaders: [
          'php-loader'
        ]
      },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
                        {
                test: /\.png$/,
                use: 'file-loader'
            }
        ]
    },
    devServer: {
  compress: true,
  stats: "errors-only",
  historyApiFallback: true
    },
    plugins: [ 
        new HtmlWebpackPlugin({
        title: 'Didžiausi miestai',
        hash: true,
        filename: 'index.html',
        template: './src/content/index.html',
    }),
        new ExtractTextPlugin({
            filename: 'app.css',
            disable: false,
            allChunks: true
        })
    ]
}