var axios = require('axios');

module.exports = {
    fetchCities: function(){
        var URI = window.encodeURI('/Home/getMiestai');

        return axios.get(URI).then(function(response){
            return response.data.data.miestai;
        })
    },
    fetchVietos: function(){
        var URI = window.encodeURI('/Home/getLankytinosVietos');

        return axios.get(URI).then(function (response){
            return response.data.data.lankVietos;
        })
    }
}