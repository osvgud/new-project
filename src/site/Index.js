const css = require('../app.scss');

import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, NavLink } from "react-router-dom";

import Header from './Header';
import Footer from './Footer';
import NavMenu from './NavMenu';


ReactDOM.render((
    <HashRouter >
    <div>

    <Header />
    <NavMenu />
          <Footer />
    </div>
    </HashRouter>
    
    ),
    document.getElementById('roots')

    
)