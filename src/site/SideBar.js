import React from 'react';
const css = require('../app.scss');

class SideBar extends React.Component {
  render(){

  return (
    <div id="Quick-navigation">

        <nav className="Navigation">   
          <div id="letsMoveToVilnius"  >Vilnius</div>
          <div id="letsMoveToKaunas">Kaunas</div>
          <div id="letsMoveToKlaipeda">Klaipėda</div>
          <div id="letsMoveToSiauliai">Šiauliai</div>
          <div id="letsMoveToPanevezys">Panevėžys</div>
        </nav>
    </div>
  )}
}

  export default SideBar;