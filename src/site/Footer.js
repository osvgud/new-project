import React from 'react';

const divStyle = {
  color: 'blue',
};

const Footer = () => {
  return (
    <div id="footer">
      Copyright &copy; OSVALDAS | KAMILĖ | <a href="http://www.html5webtemplates.co.uk">Simple web templates by HTML5</a>
    </div>      
  )
  }

export default Footer;
