import React from 'react';
var ReactRouter = require('react-router-dom');
import { Router, Route, NavLink } from "react-router-dom";
var Switch = ReactRouter.Switch;
import Site from '../content/Site';
import Miestai from '../miestai/Miestai'
import ContactContent from '../content/ContactContent'

const NavMenu = () => {
  return (
    <div>
        <ul className="men">
        <li><NavLink exact activeClassName='active' to="/" >Pradžia</NavLink></li>
        <li><NavLink exact activeClassName='active' to="/Miestai">Miestai</NavLink></li>
        <li><NavLink exact activeClassName='active' to="/ContactContent">Kontaktai</NavLink></li>
        </ul>   

    <ul>
    <Switch>
    <Route exact path="/" component={Site} />
     <Route path="/Miestai" component={Miestai} />
     <Route path="/ContactContent" component={ContactContent} />
     <Route render={function(){
         return <p className="error">Page Not Found</p>
     }}/>
     </Switch>
     </ul>
     </div>
  )
}

export default NavMenu;
