import React from 'react';
const css = require('../app.scss');
import SideBar from '../site/SideBar';


class IndexContent extends React.Component {

  render() {
    window.onload = function (e) {
      
      e.preventDefault();
      document.getElementById("letsMoveToKaunas").addEventListener("click", function () {
        document.getElementById('Kaunas').scrollIntoView();
      }, false);
      e.preventDefault();
      document.getElementById("letsMoveToVilnius").addEventListener("click", function () {
        document.getElementById('Vilnius').scrollIntoView();
      }, false);
      e.preventDefault();
      document.getElementById("letsMoveToKlaipeda").addEventListener("click", function () {
        document.getElementById('Klaipeda').scrollIntoView();
      }, false);
      e.preventDefault();
      document.getElementById("letsMoveToSiauliai").addEventListener("click", function () {
        document.getElementById('Siauliai').scrollIntoView();
      }, false);
      e.preventDefault();
      document.getElementById("letsMoveToPanevezys").addEventListener("click", function () {
        document.getElementById('Panevezys').scrollIntoView();
      }, false);
    };

    return (
      <div id="page">
        <SideBar />

        <div id="site_content">

          <div className="sections">
            <section id="Vilnius" className="overlay">
              <a href="#Miestai">Vilnius</a>
            </section>
            <section id="Kaunas" className="overlay">
              <a href="#Miestai">Kaunas</a>
            </section>
            <section id="Klaipeda" className="overlay">
              <a href="#Miestai">Klaipėda</a>
            </section>
            <section id="Siauliai" className="overlay">
              <a href="#Miestai">Šiauliai</a>
            </section>
            <section id="Panevezys" className="overlay">
              <a href="#Miestai">Panevėžys</a>
            </section>
          </div>

        </div>
      </div>
    )
  }
}

export default IndexContent;