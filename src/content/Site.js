const css = require('../app.scss');

import React from 'react';
import ReactDOM from 'react-dom';

import IndexContent from './IndexContent';

var Site = (props) => {
    return (
    <div id="page">
    <IndexContent />
    </div>
    );
}

export default Site;