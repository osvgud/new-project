import React from 'react';
const css = require('../app.scss');

import SideBar from '../SideBar';

const Siauliai = () => {
  function handleClick(e) {
    e.preventDefault();
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    modal.style.display = "block";
    modalImg.src = img.src;
    captionText.innerHTML = img.alt;
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
  }
    $(document).ready(function(){
    $(this).scrollTop(0);
});
return (
<div id="site_content">
<div className="content">
        <div className="Image">
            <img onClick={handleClick} id="myImg" src="http://www.briedis.lt/out/pictures/1/3331_siauliai-herbas.png?v3" alt="Šiaulių miesto herbas" width="180" height="200"></img>
        </div>

        <div id="myModal" className="modal">
            <span className="close">×</span>
            <img className="modal-content" id="img01"></img>
        <div id="caption"></div>
        </div>
    <h2>Šiaulių istorija</h2>
        <p>XIII-XIV a.</p>

        <p>1236 m. Šiaulių vietovardis "Soule" pirmą kartą paminėtas rašytiniuose šaltiniuose - Eiliuotoje Livonijos kronikoje, aprašančioje Saulės mūšį.
             1236 m. rugsėjo 22 diena tapo Šiaulių miesto gimimo diena.</p>

        <p>Šiaulių žemė pirmą kartą paminėta Livonijos akte 1254 m.
            XIII a. pr. jau funkcionavo lietuvių žemių konfederacija, susidariusi iš teritorinių vienetų - žemių. XIII-XIV a. rašytiniuose šaltiniuose Livonijos kronikose: Eiliuotoje Livonijos, Henriko Latvio, Hermano Vartbergės - ne kartą minimas Šiaulių žemės vardas.
            Tuometinė Šiaulių žemė - Žemaitijos žemių konfederacijos rytinis pakraštys - priklausė kunigaikščiams Bulioniams ( Vismantui, Edvikui, Spudeikai).
            Šiaulių žemė, sudėtinė Žemaičių žemių konfederacijos dalis, suvaidino svarbų vaidmenį, atremiant Livonijos puolimus XIII-XIV a. </p>

        <p>XV-XVI a.</p>

        <p>Šiauliai - miestas, perėmęs Šiaulių žemės vardą, priskirtinas seniausių XV a. pr. įsikūrusių Lietuvos miestų grupei. 
            Po Žalgirio mūšio išnyko nuolatinių kovų su Kryžiuočių ordinu grėsmė. 
            Šiaulių žemės pilys su gyvenvietėmis, tarp jų ir Salduvės pilis, neteko gynybinės reikšmės. 
            Šiaulių miestelis kūrėsi ūkiniu požiūriu svarbioje vietoje, patogioje prekybos kelių sankryžoje, aukštumoje į pietvakarius nuo Talšos ežero, t.y. dabartinio miesto centre.</p>

        <p>1524 m. Šiaulių miestas pirmą kartą paminėtas istorijos šaltiniuose - Lietuvos Didžiojo kunigaikščio Žygimanto Senojo rašte. 
            Miestelis pažymėtas 1555 m. Kasparo Vopelos sudarytame Europos žemėlapyje ( ten jis vadinamas Sovli).</p>

        <p>Dėl istorijos šaltinių stokos negalima apibūdinti tuometinio miestelio dydžio ir vaizdo. 
            Tačiau žinomas medinės bažnyčios buvimo faktas 1445 m. rodo, kad jau XV a. Šiauliai buvo stambi gyvenvietė, įgijusi krikščioniškojo miesto vaizdą.</p>

        <p>Tuometinių Šiaulių vaizdas toks - miestelis medinis, vienaaukštis, besistatantis pagal savaimingai susidariusį radialinį planą. 
            Vienintelis architektūros akcentas tuomet buvo bažnyčia - iki XVII a. pr. medinė, o nuo 1634 m. mūrinė. 
            Kitas svarbus urbanistinis miesto elementas buvo Šiaulių dvaras, kurio sodyba fragmentiškai išliko iki mūsų dienų. 
            Miestas, kaip Šiaulių ekonomijos administracinis centras, kitų ekonomijos miestelių ( Joniškio, Žagarės, Radviliškio, Gruzdžių) atžvilgiu nedominavo nei ūkiniu atžvilgiu, nei savo dydžiu, nei gyventojų skaičiumi.
            Statistikos duomenys liudija netgi atsilikimą ( pvz.: prieš 1649 m. gaisrą Šiauliuose surašyta 120 namų, Joniškyje - 183, Naujojoje Žagarėje - 110 (su Senąja Žagare - apie 155).</p>

        <p>XVI a. Šiauliai - jau Šiaulių valsčiaus administracinis centras. 
            Įsteigus karaliaus stalo valdą - Šiaulių ekonomiją (toliau ŠE), miestas tapo jos administraciniu centru. 
            Per savo istorinės raidos šimtmečius miestas išlaikė XVI a. įgytą administracinio-teritorinio centro tradiciją.
            Šiauliai buvo Šiaulių ekonomijos, Žemaitijos kunigaikštystės Šiaulių dalies teismų rezidencija, Šiaulių pavieto centras, nuo XIX a. Šiaulių apskrities centras. 
            Istoriškai Šiauliams atiteko ir dabartinis Šiaurės Lietuvos regiono centro, neoficialus Šiaurės Lietuvos sostinės vardas.</p>

        <p>XVII -XVIII a.</p>

        <p>XVII a. vidurys - XVIII a. pirma pusė buvo kataklizmų metai Lietuvos valstybėje: XVII a. vidurio karai su Švedija ir Rusija, švedų okupacija Šiauliuose, epidemijų banga. XVIII a. pradžia panaši - Šiaurės karas ir itin didelės maro epidemijos. 
            Visa tai turėjo neigiamos įtakos miesto vystymuisi. 
            Situaciją iliustruoja Dviejų Tautų Respublikos vadovo Augusto II universlas, kuriame Šiaulių būklė apibūdinama taip: "(…) anksčiau vyraujantis ekonomikoje Šiaulių miestas, dabar ir kaimo nevertas, daugiau tuščių namų, nei gyventojų yra."</p>

        <p>XVIII a. antroje pusėje, o tiksliau 1765 m., LDK karališkųjų ekonomijų administratoriumi tapo Antanas Tyzenhauzas, vienas labiausiai išprususių to laikmečio veikėjų. 
            Karališkose ekonomijose, tarp jų ir Šiaulių, jis pradėjo radikalias ekonomines ir urbanistines reformas. 
            ŠE jis numatė iš esmės perstatyti Šiaulių ir Joniškio miestus. 
            Tuomet Lietuvoje jau plito klasicizmo idėjos. 
            Jomis ir rėmėsi A. Tyzenhauzo planai - radialinio plano miestą perstatyti į taisyklingą stačiakampį. 
            1774 m. pradėta miesto centro rekonstrukcija, pertvarkant centrinę aikštę ir gatvių tinklą pagal klasicistinės architektūros principus.</p>

        <p>Antanas Tyzenhauzas (1733-1785). 
            Lietuvos Didžiosios kunigaikštystės rūmų iždininkas
            ir karališkųjų ekonomijų administratorius.</p>

        <p>Per neilgą savo valdymo laikotarpį A. Tyzenhauzas (nušalintas 1780 m.) nespėjo įgyvendinti visų savo sumanymų. 
            Pradėtas miesto plėtros procesas jau buvo nesustabdomas. 
            1783 m. karalius Stanislovas Augustas patvirtino naują Šiaulių centro perstatymo planą, ir statybos vyko toliau. 
            A. Tyzenhauzo pradėtos urbanistinės reformos keitė Šiaulių miesto vaizdą. 
            XVIII a. pab. Šiauliai iš netvarkingo medinio miestelio augo į taisyklingo stačiakampio miestą su keliasdešimčia mūrinių namų. 
            Paminėtini klasicistinio stiliaus statiniai: trijų aukštų mūrinis bajorų teismo pastatas prie bažnyčios, austerija (užeigos namai) turgaus aikštėje, dviejų aukštų mūrinis paštas. 
            Miestiečių gyvenamieji namai buvo statomi pagal tris ar keturis tipinius projektus iš plytų, iš medžio, iš medžio su plytų siena gatvės pusėje.</p>

        <p>XVIII a. pab. miesto istorijai ypač reikšminga - 1791 m. lapkričio 9 d. Lietuvos ir Lenkijos valdovas Stanislovas Augustas savo privilegija galutinai įtvirtino Šiaulių miesto savivaldą. 
            Privilegijos originalas sudegė Pirmojo pasaulinio karo metu. 
            Tačiau išliko jam lygiavertis šaltinis - įrašas Lietuvos Metrikoje su spalvotu Šiaulių miesto herbo piešiniu.</p>

        <p>1791 m. lapkričio 9 d. Lietuvos ir Lenkijos valdovas Stanislovas Augustas Šiauliams patvirtino laisvojo miesto teises ir pirmąjį pačių šiauliečių išsirinktą herbą.</p>
        
        <h2>Lankytinos vietos</h2>
        
        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://www.kryziukalnas.lt/">
            <img src="http://g3.dcdn.lt/images/pix/kryziu-kalnas-69550346.jpg" alt="Kryžių kalnas" width="600" height="400"></img>
            </a>
            <div className="desc">Kryžių kalnas</div>
        </div>
        </div>


        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://www.sokoladomuziejus.lt/">
            <img src="http://tic.siauliai.lt/wp-content/uploads/2015/06/sokolado_muziejus_fasadas.jpg" alt="Šokolado muziejus" width="600" height="400"></img>
            </a>
            <div className="desc">Šokolado muziejus</div>
        </div>
        </div>

        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://www.ausrosmuziejus.lt/Ekspozicijos/Dviraciu-muziejus">
            {/*<img src="http://www.visitlithuania.net/images/stories/Miestai_pazaislio_vienuolynas.jpg" alt="Northern Lights" width="600" height="400"></img>*/}
            <img src="http://www.skrastas.lt/galery/_skrastas/2015/05/12/Dviraciu_muziejus__03_GBs.jpg" alt="Dviračių muziejus" width="600" height="400"></img>
            </a>
            <div className="desc">Dviračių muziejus</div>
        </div>
        </div>

        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://tic.siauliai.lt/place/saules-laikrodzio-aikste/">
            <img src="https://trumpalaikenuoma.lt/uploads/places/0/14_53_xLvozk65_big.jpg" alt="Saulės laikrodžio aikštė" width="600" height="400"></img>
            </a>
            <div className="desc">Saulės laikrodžio aikštė</div>
        </div>
        </div>

        <div className="clearfix"></div>       

      </div>
      </div>
)}

export default Siauliai;