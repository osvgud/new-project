import React from 'react';
const css = require('../app.scss');

import SideBar from '../SideBar';

const Panevezys = () => {
  function handleClick(e) {
    e.preventDefault();
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    modal.style.display = "block";
    modalImg.src = img.src;
    captionText.innerHTML = img.alt;
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
  }
    $(document).ready(function(){
    $(this).scrollTop(0);
});
return (
<div id="site_content">
<div className="content">
    
        <div className="Image">
            <img onClick={handleClick} id="myImg" src="http://www.briedis.lt/out/pictures/1/3333_panevezys-herbas.png?v3" alt="Panevėžio miesto herbas" width="180" height="200"></img>
        </div>

        <div id="myModal" className="modal">
            <span className="close">×</span>
            <img className="modal-content" id="img01"></img>
        <div id="caption"></div>
        </div>

    <h2>Panevėžio istorija</h2>
        <p>Panevėžys įsikūręs senojoje Upytės žemėje, minimoje jau XIII a. viduryje. Karų su kalavijuočiais metais Upytės žemė, buvusi tuometės Lietuvos pasienyje, buvo nuolat niokojama. Tai viena priežasčių, kodėl miestų vėlyvaisiais viduramžiais šiame regione nebuvo.
            Pasibaigus kovoms su Livonijos ordinu, tankiau apgyvendinti gerokai ištuštėję pasienio ruožai. 
            Gyventojų  ėmė sparčiai gausėti. XV a. antrojoje pusėje - XVI a. pradžioje įsikūrė dauguma dabar Šiaurės rytų Lietuvoje esančių miestų.
             XVI a. pradžioje įkurtas ir Panevėžys.</p>

        <p>Seniausias iš dabar žinomų dokumentų, kuriame neabejotinai kalbama apie Panevėžį, yra Lietuvos didžiojo kunigaikščio ir Lenkijos karaliaus Aleksandro 1503 metų rugsėjo 7 dienos raštas.
            Juo didysis kunigaikštis dovanojo žemę tarp Lėvens ir Nevėžio upių Ramygalos klebonui su sąlyga, kad šioje teritorijoje būtų pastatyta bažnyčia, kuri kovotų su krašte dar labai gaja pagonybe.
            Dovanotoje žemėje dešiniajame Nevėžio krante (netoli dabartinių Senamiesčio ir Venslaviškio gatvių sankryžos) buvo pastatyta nedidelė medinė bažnyčia ir klebonija. 
            Priešais bažnyčią buvo turgaus aikštė, joje stovėjo karčema ir aludė (bravoras) su pirtimi. 
            Šiai gyvenvietei vėliau prigijo Senojo Panevėžio vardas.</p>

        <p>XVI a. pradžioje kairiajame Nevėžio krante, valstybinio  dvaro  teritorijoje,  įsikūrė  gyvenvietė,  kuri  pradėta  vadinti Naujuoju Panevėžiu. 
            Ji greitai augo ir netrukus pralenkė bažnyčios žemėje įsikūrusį miestelį. 
            Aplink Naujojo Panevėžio centre buvusią trikampę Turgaus aikštę (dabar Laisvės aikštė) buvo išsidėstę miestiečių sklypai. 
            Manoma, kad XVI a. viduryje galėjo būti apie 350 gyventojų. Daugelis jų vertėsi žemdirbyste, kiti - amatais. 
            Ir pagal dydį, ir pagal socialines ekonomines charakteristikas Panevėžys atitiko to meto Lietuvos vidutinio miestelio lygį. 
            1565-1566 metais Lietuvoje vykstant administracinei reformai Panevėžys tapo apskrities (pavieto) centru. 
            Tai miesteliui suteikė tolesnio augimo perspektyvą.</p>

        <p>XVII-XVIII a. Lietuvoje buvo nepalankūs augti miestams. 
            Vyko nuolatiniai karai, siautėjo epidemijos, grėsė badas. 
            Panevėžys tuo metu nesiplėtė. 1614 metais mieste pastatytas pirmasis mūrinis pastatas - teismo archyvas. 
            1727 metais Panevėžyje įsikūrė vienuoliai pijorai, kurie netrukus atidarė kolegiją - pirmąją vidurinę mokyklą mieste. 
            Ji neprilygo garsėjusioms Kražių ar Vilniaus mokykloms, bet vis tiek Panevėžys tapo nemažo regiono švietimo centru. 
            XVIII a. antrojoje pusėje miestas, išlikęs privačių savininkų rankose, gavo keletą privilegijų. 
            Sprendžiant iš netiesioginių šaltinių, nuo 1791-1792 metų Panevėžys turėjo savivaldą.</p>

        <p>XVIII a. antrojoje pusėje Panevėžio statusas balansavo tarp miestelio ir miesto.
            Kitame šimtmetyje ji buvo ryžtingai peržengta.</p>

        <p>XIX amžiuje, pakankamai ekonomiškai ir politiškai stabiliame, susidarė gana palankios sąlygos ir Panevėžio miesto plėtrai. 
            Tik tris kartus - 1812 m. pražygiuojant Napoleono armijai bei per 1831 ir 1863 metų sukilimus - miestas patyrė žymesnių sukrėtimų, kurių padariniai buvo greitai likviduoti. 
            1811 metais Panevėžys rusų valdžios buvo išpirktas iš privataus savininko, tai pagerino jo ekonominę ir politinę padėtį. 
            Pagrindinis tolesnių dviejų miesto istorijos šimtmečių bruožas - nuolatinis ir greitas augimas,  pasireiškęs gyventojų  daugėjimu bei ekonomikos, infrastruktūros, socialinės sferos ir kultūros plėtra. </p>

        <p>Manoma, kad XIX a. pradžioje Panevėžyje gyveno apie 800 žmonių, taigi maždaug per 250 metų gyventojų skaičius išaugo tik du kartus. 
            Maždaug nuo 1825 metų netolygiai, bet nuolat panevėžiečių ėmė daugėti.
            1897 metais mieste jau buvo 13 000 gyventojų , taigi per šimtmetį padaugėjo daugiau kaip 16 kartų!</p>

        <p>XIX amžiaus pabaigoje mieste vyko pramonės perversmas - atsirado pirmosios modernios įmonės, pradėti naudoti mechaniniai varikliai, gamyba buvo orientuota į platesnes rinkas. 
            Stiprėjo kredito sistema, plėtėsi prekybos apimtys ir atstumai. Vyko luomų niveliacija, mieste buvo aktyvus lietuvių, lenkų ir žydų tautinis judėjimas. Plėtėsi švietimo sistema ir didėjo raštingumas.</p>

        <p>XX amžiaus pradžioje Panevėžyje išryškėjo pirmieji pilietinės visuomenės bruožai. 
            Iš esmės XIX amžiaus pabaigoje - XX amžiaus pradžioje Panevėžys tapo tas, kas yra ir dabar - dideliu regioniniu ekonomikos ir kultūros centru, jau tuomet pagal svarbą užėmusiu maždaug ketvirtą vietą Lietuvoje (be Klaipėdos).</p>

        <p>Tarpukariu (1918-1939 m.) Panevėžys dar labiau išaugo. 
            Toliau plėtojosi ekonomika, švietimo ir kultūros sferos, plėtėsi teritorija, daugėjo gyventojų. 
            1923-1939 metais gyventojų skaičius mieste padidėjo nuo 19 200 iki 26 600. 
            Svarbiausias šios epochos bruožas - miesto lietuvėjimas.
            Nors kitataučių  mieste tarpukario metais liko daug (1923 metais lietuviai sudarė 53% miestiečių), vis daugėjo lietuvių. 
            Šiam procesui didžiulės įtakos turėjo valstybės politika, valdymo institucijų ir svarbiausia - švietimo sistemos lietuvinimas. 
            Kūrėsi lietuvių miestiečių tradicijos, gyvenimo būdas. 
            Miesto visuomeniniam gyvenimui savitumo teikė aktyvi lenkų ir žydų tautinių bendrijų veikla.</p>

        <p>Skausmingi Panevėžiui buvo Antrojo pasaulinio karo metai.
            1940 m. birželio 15 d. miestą užėmė rusų kariuomenė. 
            Nemažo atgarsio susilaukė sovietų teroro aktai: 1941 m. birželio 25 d. 
            žvėriškai nukankinti gydytojai S.Mačiulis, J.Žemgulis, A.Gudonis bei medicinos sesuo Z. Kanevičienė, politinių kalinių žudynės prie cukraus fabriko. 
            Vokiečių okupacijos metais nužudyta per 8000 miesto ir jo apylinkių gyventojų, kurių daugumą sudarė žydai. 
            Neramus panevėžiečiams buvo ir pokaris. 
            Daug jų nukentėjo nuo represijų, patyrė tremtinių dalią.</p>

        <p>Po Antrojo pasaulinio karo natūrali Panevėžio raida buvo sutrikdyta. 
            Miesto gyvenimui stipriai įtakos turėjo komunistų partijos ir sovietinių valdžios įstaigų direktyvinis vadovavimas. 
            Miestas formuotas kaip didelis pramonės centras. 
            Panevėžyje per šeštąjį-aštuntąjį dešimtmečius pastatyta daug didelių pramonės įmonių, todėl vėl labai sparčiai daugėjo gyventojų.
            1959-1979 metais panevėžiečių padaugėjo nuo 41 000 iki 101 500.
            1979-1990 metais gyventojų užfiksuota jau 130 000.
            Plėtėsi miesto teritorija, išaugo gyvenamųjų namų kvartalai.
            Septintajame dešimtmetyje pradėta miesto centrinės dalies rekonstrukcija, sutvarkyta Nevėžio senvagė.</p>

        <p>1990 metais Lietuvai atgavus nepriklausomybę prasidėjo greitas visuomenės integravimas į šiuolaikinę Europos bendruomenę. 
            Jis vyksta visose - mentaliteto, ekonomikos, papročių, elgesio normų, kultūros ir švietimo modelių -  sferose. 
            Didžiausias išbandymas teko miesto pramonei, kuri turėjo labai greitai restruktūrizuotis ir prisitaikyti prie poindustrinės visuomenės diktuojamų sąlygų. 
            Nepaisant paskutinio dešimtmečio raidos netolygumų, Panevėžys išlieka didelis regiono centras, jau daugiau kaip šimtmetį vadinamas penktuoju Lietuvos miestu.</p>

        <h2>Lankytinos vietos</h2>
        
        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://www.leliuvezimoteatras.lt/">
            <img src="http://g4.dcdn.lt/images/pix/file59113041_956ab56.jpg" alt="Panevėžio Lėlių vežimo teatras" width="600" height="400"></img>
            </a>
            <div className="desc">Panevėžio Lėlių vežimo teatras</div>
        </div>
        </div>


        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://www.siaurukas.eu/">
            <img src="https://aina.lt/wp-content/uploads/2015/12/Siauruko_stotis-640x416.jpg" alt="Aukštaitijos siaurasis geležinkelis" width="600" height="400"></img>
            </a>
            <div className="desc">Aukštaitijos siaurasis geležinkelis</div>
        </div>
        </div>

        <div className="clearfix"></div>       


      </div>
      </div>
)}

export default Panevezys;