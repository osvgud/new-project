import React from 'react';
const css = require('../app.scss');

import SideBar from '../SideBar';

const Vilnius = () => {
  function handleClick(e) {
    e.preventDefault();
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    modal.style.display = "block";
    modalImg.src = img.src;
    captionText.innerHTML = img.alt;
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
  }
    $(document).ready(function(){
    $(this).scrollTop(0);
});
return (
<div id="site_content">
<div className="content">
        <div className="Image">
            <img onClick={handleClick} id="myImg" src="http://www.briedis.lt/out/pictures/1/3325_vilnius-herbas.png?v3" alt="Vilniaus miesto herbas" width="180" height="200"></img>
        </div>

        <div id="myModal" className="modal">
            <span className="close">×</span>
            <img className="modal-content" id="img01"></img>
        <div id="caption"></div>
        </div>
    <h2>Vilniaus istorija</h2>
        <p>Lietuva – seniausia Baltijos šalių valstybė.
            XIII a. lietuvių gentis galutinai suvienijęs kunigaikštis Mindaugas pasirinko kryptį į senos kultūros Europą: 
            1251 m. popiežius paskelbė, kad Lietuva yra Europos karalystė.</p>

        <p>XIII–XV a. Lietuvos Didžioji Kunigaikštystė išsiplėtė į didelę viduramžių valstybę, kurios teritorija apėmė didžiąją dalį Rytų Europos.
             XVI a. – Vilniaus suklestėjimo laikotarpis: tuo metu sostinė tampa vienu iš gražiausių Rytų Europos miestų, kurio palikimu žavimės ir šiandien.</p>

        <p>Lietuvos istorija yra labai dramatiška: iš 1000 metų net 700 metų vyko karai, okupacijos. 
            Stiprus laisvės troškimas net du kartus Lietuvą padarė nepriklausomą – 1918 m. ir „dainuojanti revoliucija“ – 1990 m. 
            Dabar Lietuva – aktyvi ir perspektyvi ES narė.</p>

         <h2>Lankytinos vietos</h2>
        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://www.valdovurumai.lt">
            <img src="http://www.vilnius-tourism.lt/wp-content/uploads/2011/09/IMG_3838.jpg" alt="Valdovų rūmai" width="600" height="400"></img>
            </a>
            <div className="desc">Valdovų rūmai</div>
        </div>
        </div>


        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://www.lnm.lt">
            {/*<img src="https://Miestaizoo.blob.core.windows.net/bcms/image/d51a6ff4a61c48c48f9bf3ee6b024b17/ruonis2.jpg" alt="Forest" width="600" height="400"></img>*/}
            <img src="http://www.vilnius-tourism.lt/wp-content/uploads/2011/09/IMG_1286-20150531.jpg" alt="Gedimino pilies bokštas" width="600" height="400"></img>
            </a>
            <div className="desc">Gedimino pilies bokštas</div>
        </div>
        </div>

        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://www.katedra.lt/">
            <img src="https://sophiesophrosyne.files.wordpress.com/2014/10/senamiestis-46.jpg" alt="Arkikatedra bazilika" width="600" height="400"></img>
            </a>
            <div className="desc">Arkikatedra bazilika</div>
        </div>
        </div>

        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://www.ausrosvartai.lt">
            <img src="http://www.vilnius-tourism.lt/wp-content/uploads/2011/09/IMG_4337.jpg" alt="Aušros vartai" width="600" height="400"></img>
            </a>
            <div className="desc">Aušros vartai</div>
        </div>
        </div>

        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://www.vilnius-tourism.lt/wp-content/uploads/2011/09/gera002.jpg">
            <img src="http://www.vilnius-tourism.lt/wp-content/uploads/2011/09/gera002.jpg" alt="Trijų kryžių paminklas" width="600" height="400"></img>
            </a>
            <div className="desc">Trijų kryžių paminklas</div>
        </div>
        </div>

        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://www.onosbaznycia.lt">
            <img src="http://www.vilnius-tourism.lt/wp-content/uploads/2011/09/IMG_3995.jpg" alt="Šv. Onos bažnyčia" width="600" height="400"></img>
            </a>
            <div className="desc">Šv. Onos bažnyčia</div>
        </div>
        </div>

        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://www.muziejus.vu.lt">
            <img src="http://www.vilnius-tourism.lt/wp-content/uploads/2011/10/Vilnius-University.jpg" alt="Vilniaus universitetas" width="600" height="400"></img>
            </a>
            <div className="desc">Vilniaus universitetas</div>
        </div>
        </div>

        <div className="clearfix"></div>        

      </div>
    </div>      
)}

export default Vilnius;