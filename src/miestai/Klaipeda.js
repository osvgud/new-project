import React from 'react';
const css = require('../app.scss');

import SideBar from '../SideBar';

const Klaipeda = () => {
  function handleClick(e) {
    e.preventDefault();
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    modal.style.display = "block";
    modalImg.src = img.src;
    captionText.innerHTML = img.alt;
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
  }
    $(document).ready(function(){
    $(this).scrollTop(0);
});
return (
<div id="site_content">
<div className="content">
    <div className="Image">
            <img onClick={handleClick} id="myImg" src="http://www.briedis.lt/out/pictures/1/3329_klaipeda-herbas.png?v3" alt="Klaipėdos miesto herbas" width="180" height="200"></img>
        </div>

        <div id="myModal" className="modal">
            <span className="close">×</span>
            <img className="modal-content" id="img01"></img>
        <div id="caption"></div>
    </div>
    <h2>Klaipėdos istorija</h2>
        <p>Klaipėda – trečiasis pagal dydį Lietuvos miestas, įsikūręs Kuršių marių ir Baltijos jūros susiliejimo vietoje, Pajūrio žemumoje prie Dangės žiočių. 
            Miestas yra Klaipėdos apskrities centras, dirba Klaipėdos miesto savivaldybė, veikia Kuršių nerijos nacionalinio parko ir Pajūrio regioninio parko direkcijos, 17 pašto skyrių.</p>

        <p>Per miestą teka trys upės: Akmena (Dangė), Smeltalė ir Kretainis. Nuo sostinės Vilniaus Klaipėda nutolusi 311 km į šiaurės vakarus.</p>

        <p>Pagrindiniai susisiekimo taškai yra Klaipėdos geležinkelio stotis, autobusų stotis, jūrų uostas. 
            Klaipėdoje yra kelios dešimtys viešbučių, 10 svečių namų, 6 katalikų bažnyčios, 3 stačiatikių cerkvės, 2 sentikių cerkvės, 1 sinagoga, 1 evangelikų liuteronų bažnyčia, 5 muziejai, 4 teatrai ir 15 sporto centrų.</p>

        <p>Oficiali miesto diena – rugpjūčio 1-oji (miesto gimtadienis), kasmet liepos mėnesio pabaigoje mieste vyksta Jūros šventė, taip pat Klaipėdą garsina tarptautinis Pilies džiazo festivalis.</p>

        <p>Didžioji miesto dalis yra ryčiau jūros ir marių. 
            Klaipėdoje yra dvi perkėlos: Senoji (miesto centre) ir Naujoji (į pietus nuo centro). 
            Išilgai Kuršių marių yra uostas, kurio šiaurinis kraštas siekia Baltijos jūrą, o pietinis baigiasi ties Malkų prieplauka – Kuršių marių įlankėle, esančia ties Kiaulės Nugaros sala; čia yra tarptautinė jūrų perkėla. </p>

        <p>Klaipėdoje yra unikalus Jūrų muziejus ir delfinariumas. 
            Šiauriau Klaipėdos išlikusios natūralios kopos, veda pėsčiųjų ir dviračių takai į Palangą.</p>

        <p>1945 m. sausio 28 d. Klaipėdą užėmė sovietų kariuomenė ir perdavė Lietuvos SSR. 
            Klaipėda prie Lietuvos prijungta pagal atskirą aktą, kuriuo visos Vokietijos užimtos teritorijos nuo 1937 m. pripažintos užgrobtomis neteisėtai. 
            Per Antrąjį pasaulinį karą kai kurios miesto dalys labai nukentėjo nuo sovietų aviacijos bombardavimų bei vokiečių įgulos sprogdinimų. 
            Karo pabaigoje vykstant masinei senųjų Klaipėdos gyventojų evakuacijai, miestas neteko senosios septynių šimtmečių vokiečių-lietuvių-prūsų koegzistencijos tradicijų.</p>

        <p>1946 m. prie Klaipėdos miesto prijungtos gyvenvietės Giruliai ir Melnragė.</p>

        <p>Iki 7-8 dešimtmečių sandūros Klaipėda buvo kuriama naujakurių iš visos SSRS, nepaisant miesto istorijos arba priešpriešinant save jai. 
            Kelis pirmuosius pokario dešimtmečius Klaipėda atliko iš esmės ekonominės SSRS provincijos su itin išvystyta žvejybos pramone vaidmenį. 
            Per jūrų uostą geležinkelio keltu Klaipėda–Mukranas buvo plukdomi sovietų strateginiai kroviniai į Rytų Vokietiją.</p>

        <p>Klaipėda yra svarbus Vakarų Lietuvos ekonomikos centras. 
            Klaipėdos uostas sėkmingai dirba ir konkuruoja su rytinėje Baltijos pakrantėje esančiais uostais (Ryga, Liepoja, Talinas), pastaraisiais metais uoste didėja priimamų kruizinių laivų skaičius.</p>

      <h2>Lankytinos vietos</h2>
        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://muziejus.lt/lt/delfinariumas">
            <img src="https://s1.15min.lt/images/photos/2013/12/10/original/delfinai-ruosiasi-susitikimui-su-ziurovais-52a70f095361d.jpg" alt="Delfinariumas" width="600" height="400"></img>
            </a>
            <div className="desc">Delfinariumas</div>
        </div>
        </div>


        <div className="responsive">
        <div className="gallery">
            <a target="_blank" href="http://muziejus.lt/">
            {/*<img src="https://Miestaizoo.blob.core.windows.net/bcms/image/d51a6ff4a61c48c48f9bf3ee6b024b17/ruonis2.jpg" alt="Forest" width="600" height="400"></img>*/}
            <img src="http://www.morenahotel.lt/wp-content/uploads/2015/10/juru-muziejus-klaipeda.jpg" alt="Lietuvos jūrų muziejus" width="600" height="400"></img>
            </a>
            <div className="desc">Lietuvos jūrų muziejus</div>
        </div>
        </div>

        <div className="clearfix"></div>     

      </div>
      </div>
)}

export default Klaipeda;