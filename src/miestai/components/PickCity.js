import React from 'react';
import PropTypes from 'prop-types';
const {Link, IndexLink} = require('react-router')
var ReactRouter = require('react-router-dom');
import { Router, Route, NavLink } from "react-router-dom";

const css = require('../../app.scss');

function PickCity(props){
            var cities = ['Vilnius','Kaunas','Šiauliai','Panevėžys','Klaipėda'];
            return(

                <ul className='cities'>
                    {cities.map(function(cit) {
                        return(
                            <li
                            style={cit === props.selectedCity ? {color: '#d0021b'}: null}
                            onClick={props.onSelect.bind(null,cit)}
                             key={cit}>
                             {cit}
                             </li>
                        )
                    })}
                </ul>
            )
}

PickCity.propTypes = {
    selectedCity: PropTypes.string.isRequired,
    onSelect: PropTypes.func.isRequired,
}

export default PickCity;