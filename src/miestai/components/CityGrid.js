import PropTypes from 'prop-types';
import React from 'react';

 function CityGrid(props){
    return(
        <div>
            <div className="title">
            Istorija
            </div>
            <div className='text-cont'>
            {props.content.istorija}
            </div>
            <div className="title">
                Lankytinos vietos
            </div>
            <ul className='lank-list'>
                {props.lank.map(function(lank){
                    return(
                    <li key={lank.id} className='lank-item'>
                        <ul className='space-list-items'>
                            <li>
                                <img className='picture'
                                src={lank.nuotrauka}
                                alt={lank.pavadinimas} />
                            </li>
                            <li><a className="vietu-link" href={lank.nuoroda}>{lank.pavadinimas}</a></li>
                        </ul>
                    </li>
                    )
                })}
            </ul>

        </div>
    )
}

CityGrid.propTypes = {
    content: PropTypes.object.isRequired,
    lank: PropTypes.array.isRequired,
}

export default CityGrid;