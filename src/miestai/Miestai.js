import React from 'react';
import PropTypes from 'prop-types';

const css = require('../app.scss');
var api = require('../utils/api');
var Link = require('react-router-dom').Link;

import CityGrid from './components/CityGrid'
import PickCity from './components/PickCity'

function getIndex(name){
    switch(name)
    {
        case "Vilnius": return 0;
        case "Kaunas": return 1;
        case "Klaipėda": return 2;
        case "Šiauliai": return 3;
        case "Panevėžys": return 4;
    }
}

class Miestai extends React.Component {
constructor (props)
{
    super(props);
    this.state = {
        selectedCity: 'Vilnius',
        content: null,
        lank: null
    };

    this.updateCity = this.updateCity.bind(this);
}

componentDidMount(){
    this.updateCity(this.state.selectedCity);
}

updateCity(cit){
    this.setState(function(){
        return{
            selectedCity: cit,
            content: null,
            lank: null
        }
    });
        api.fetchCities().then(function(content){
        this.setState(function(){
            let s = getIndex(this.state.selectedCity)
            return{
            content: content[s]
        }
        })
    }.bind(this));

        api.fetchVietos().then(function(viet){
            this.setState(function(){
                let s = getIndex(this.state.selectedCity)
                var tink = [];
                for(var m in viet)
                {
                    if(viet[m].fk_MIESTAI == s+1)
                    {tink.push(viet[m])}
                }               
                return{
                    lank: tink
                }
            })
        }.bind(this));

}

    render(){
            return(
                <div>
                <PickCity 
                selectedCity={this.state.selectedCity}
                onSelect={this.updateCity}
                />
                {!this.state.content || !this.state.lank
                    ? <p className="error">Kraunasi...</p> : <CityGrid content={this.state.content}
                        lank={this.state.lank} />
                }
                </div>
            )
        }}
export default Miestai;