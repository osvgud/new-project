﻿using dotnet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace dotnet.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult getMiestai()
        {
            ResponseModel response = new ResponseModel();
            MiestaiListModel model = new MiestaiListModel();
            using (var db = new Model1())
            {
                model.miestai = db.Miestas.Select(x => new MiestaiModel()
                {
                    pavadinimas = x.pavadinimas,
                    istorija = x.istorija,
                    id_MIESTAI = x.id_MIESTAI
                }).ToList();
            }
            response.success = true;
            response.data = model;
            return Json(response,JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult getLankytinosVietos()
        {
            ResponseModel response = new ResponseModel();
            LankVietosListModel lank = new LankVietosListModel();
            using (var db = new Model1())
            {
                lank.lankVietos = db.Lankytina_vieta.Select(y => new LankVietosModel()
                {
                    id = y.id,
                    pavadinimas = y.pavadinimas,
                    nuoroda = y.nuoroda,
                    nuotrauka = y.nuotrauka,
                    fk_MIESTAI = y.fk_MIESTAI
                }).ToList();
            }
            response.success = true;
            response.data = lank;
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

            
    }
}