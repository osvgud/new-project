namespace dotnet
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=MiestaiEntities")
        {
        }

        public virtual DbSet<Lankytina_vieta> Lankytina_vieta { get; set; }
        public virtual DbSet<Miesta> Miestas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Lankytina_vieta>()
                .Property(e => e.pavadinimas)
                .IsUnicode(false);

            modelBuilder.Entity<Lankytina_vieta>()
                .Property(e => e.nuoroda)
                .IsUnicode(false);

            modelBuilder.Entity<Lankytina_vieta>()
                .Property(e => e.nuotrauka)
                .IsUnicode(false);

            modelBuilder.Entity<Miesta>()
                .Property(e => e.pavadinimas)
                .IsUnicode(false);

            modelBuilder.Entity<Miesta>()
                .Property(e => e.istorija)
                .IsUnicode(false);

            modelBuilder.Entity<Miesta>()
                .HasMany(e => e.Lankytina_vieta)
                .WithRequired(e => e.Miesta)
                .HasForeignKey(e => e.fk_MIESTAI)
                .WillCascadeOnDelete(false);
        }
    }
}
