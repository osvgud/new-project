﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dotnet.Models
{
    public class LankVietosListModel
    {
        [JsonProperty("lankytinos_vietos")]
        public Dictionary<string, LankVietosModel> LankVietosCodes { get; set; }
        public List<LankVietosModel> lankVietos { get; set; }
    }
}