namespace dotnet
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Lankytina_vieta
    {
        public int id { get; set; }

        [Required]
        public string pavadinimas { get; set; }

        [Required]
        public string nuoroda { get; set; }

        [Required]
        public string nuotrauka { get; set; }

        public int fk_MIESTAI { get; set; }

        public virtual Miesta Miesta { get; set; }
    }
}
