﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dotnet.Models
{
    public class MiestaiListModel
    {
        [JsonProperty("miestai")]
        public Dictionary<string,MiestaiModel> MiestaiCodes { get; set; }
        public List<MiestaiModel> miestai { get; set; }
    }
}