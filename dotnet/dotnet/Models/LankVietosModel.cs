﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dotnet.Models
{
    public class LankVietosModel
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("pavadinimas")]
        public string pavadinimas { get; set; }
        [JsonProperty("nuoroda")]
        public string nuoroda { get; set; }
        [JsonProperty("nuotrauka")]
        public string nuotrauka { get; set; }
        [JsonProperty("fk_MIESTAI")]
        public int fk_MIESTAI { get; set; }
    }
}