﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dotnet.Models
{
    public class ResponseModel
    {
        public bool success { get; set; }
        public object data { get; set; }
    }
}