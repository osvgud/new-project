namespace dotnet
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Miesta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Miesta()
        {
            Lankytina_vieta = new HashSet<Lankytina_vieta>();
        }

        [Key]
        public int id_MIESTAI { get; set; }

        [Required]
        public string pavadinimas { get; set; }

        [Required]
        public string istorija { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Lankytina_vieta> Lankytina_vieta { get; set; }
    }
}
