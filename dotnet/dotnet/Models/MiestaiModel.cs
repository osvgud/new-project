﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dotnet.Models
{
    public class MiestaiModel
    {
        [JsonProperty("pavadinimas")]
        public string pavadinimas { get; set; }
        [JsonProperty("istorija")]
        public string istorija { get; set; }
        [JsonProperty("id_MIESTAI")]
        public int id_MIESTAI { get; set; }
    }
}